"             /$$
"            |__/
"  /$$    /$$ /$$ /$$$$$$/$$$$   /$$$$$$   /$$$$$$$
" |  $$  /$$/| $$| $$_  $$_  $$ /$$__  $$ /$$_____/
"  \  $$/$$/ | $$| $$ \ $$ \ $$| $$  \__/| $$
"   \  $$$/  | $$| $$ | $$ | $$| $$      | $$
"    \  $/   | $$| $$ | $$ | $$| $$      |  $$$$$$$
"     \_/    |__/|__/ |__/ |__/|__/       \_______/
"

" PLUGINS ---------------------------------------------------------------- {{{

  call plug#begin('~/.config/nvim/plugged')

  " startup screen
  Plug 'mhinz/vim-startify'

  " asynchronous commands
  Plug 'skywind3000/asyncrun.vim'
  let g:asyncrun_open = 6

  " file browser
  Plug 'preservim/nerdtree'

  " color scheme
  Plug 'gruvbox-community/gruvbox'

  " Status line
  Plug 'vim-airline/vim-airline'

  " Intellisense
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  let g:coc_global_extensions = ['coc-pairs', 'coc-sh', 'coc-git', 'coc-yaml', 'coc-json', 'coc-html', 'coc-css', 'coc-tsserver', 'coc-prettier', 'coc-clangd', 'coc-flutter', 'coc-go', 'coc-godot', 'coc-python']

  " Git
  Plug 'mhinz/vim-signify'
  let g:signify_line_highlight = 1
  Plug 'tpope/vim-fugitive'

  " Go plugin
  " Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
  " let g:go_fmt_command = goimports

  " Vim Wiki
  Plug 'vimwiki/vimwiki'

  " CSS Colors
  Plug 'ap/vim-css-color'

  call plug#end()

" }}}

" MAPPINGS --------------------------------------------------------------- {{{

" }}}

" VIMSCRIPT -------------------------------------------------------------- {{{

" Enable code folding.
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" Remove trailing spaces
function! TrimWhiteSpace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfunction

augroup trimtrail
    autocmd!
    autocmd BufWritePre * :call TrimWhiteSpace()
augroup END

" Disable compatibility with vi that can cause unexpected issues.
set nocompatible
filetype on

" Enable plugin and load plugin for detected file type.
filetype plugin on

" Turn syntax highlighting on
syntax on

" Add line numbers
set number

" Set relative numbers
set relativenumber

" Start scrolling when you are near the bottom
set scrolloff=8

" Additional column for indications
set signcolumn=yes

" Enable Buffer ~ No worries if saved or not !
set hidden

" Disable audio for error
set noerrorbells

" 80 columns rule
set colorcolumn=74

" Indentation setting
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent

" Disable line wrapping
set nowrap

" Disable swapfile ( we will use undo directory )
set noswapfile

" Disable file backup
set nobackup

" Set Undo directory ( you need to manually create this dir )
set undodir=~/.config/nvim/undodir

" Enable undofile
set undofile

" While searching through a file highlight matched string as you type
set incsearch

" Ignore capital letters while searching
set ignorecase

" Override ignorecase option if searching capital letters
set smartcase

" Enable auto completion menu after pressing TAB.
set wildmenu

" Make wildmenu behave like similar to Bash completion.
set wildmode=list:longest

" Wildmenu will ignore files with these extensions.
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

" }}}

" STATUS LINE ------------------------------------------------------------ {{{

let g:airline_section_z='%l/%L'

" enable tabline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = ' '
let g:airline#extensions#tabline#right_sep = ' '
let g:airline#extensions#tabline#right_alt_sep = ' '

" enable powerline fonts
let g:airline_powerline_fonts = 1
let g:airline_left_sep = ''
let g:airline_right_sep = ''

" Switch to your current theme
let g:airline_theme = 'gruvbox'

" Always show tabs
set showtabline=2

" Show the status on the second last line
set laststatus=2

" We don't need to see things like -- INSERT -- anymore
set noshowmode

" }}}

" COLOR SCHEME ------------------------------------------------------------ {{{

" Call the theme one
"colorscheme onedark

" Set true colors
"set termguicolors

if (has("termguicolors"))
 set termguicolors
endif

colorscheme gruvbox
" }}}

" FINDING FILES ---------------------------------------------------------------- {{{

" Tab-completion for all file-related taks
set path+=**

" Display all matching files when we tab complete
set wildmenu

" # Now We Can :
" - Hit tab to :find by partial match (recursively)
" - Hit tab to :find ./files/*.extension ( searching specific folder )
"
"   Use * to make it fuzzy
"
"
" - :b lets you autocomplete any open buffer
" }}}

" TAG JUMPINNG ---------------------------------------------------------------- {{{

" Create the `tags` file ( may need to install ctags first )
command! MakeTags !ctags -R .

" # Now We can :
" - Use  ^] to jump to tag under cursor
" - Use g^] for ambiguous tags
" - use  ^t to jump back up the tag stack
" - This doesn't help if you want a visual list of tags
" -
" }}}

" AUTOCOMPLETE ---------------------------------------------------------------- {{{
" Reference documentation in |ins-completion|

" We Can :
" - ^x^n for JUST this file
" - ^x^f for filenames
" - ^x^] for tags only
" - ^n for anything specified by completion option
" - Use ^n and ^p to go back and forth in the suggestion list
"
"
" }}}

" NERDTree --------------------------------------------------------------- {{{

" Start NERDTree when Vim starts with a directory argument.
  autocmd StdinReadPre * let s:std_in=1
  autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
    \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif

" }}}

" COC CONFIG ---------------------------------------------------------------- {{{

" Set internal encoding of vim, not needed on neovim, since coc.nvim using some
" unicode characters in the file autoload/float.vim
set encoding=utf-8

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>


" }}}
