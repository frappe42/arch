# This file is used to configure the installed arch linux for daily use.

setTime () {

# Set timezone.
ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime

# Enable network time sync.
timedatectl set-ntp true

# Set the Hardware Clock from the System Clock.
hwclock --systohc

}

setLocale () {

# Install fonts.
pacman -S noto-fonts --noconfirm

# Uncomment required locales from '/etc/locale.gen'.
sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen

# Generate locale.
locale-gen

# Set system locale. (creates locale.conf).
localectl set-locale LANG=en_US.UTF-8

}

setNetworkConfig () {

# Create the hostname file.
echo "Specify hostname. This will be used to identify your machine on a network."
read hostName; echo $hostName > /etc/hostname

# Add matching entries to '/etc/hosts'.
# ( If the system has a permanent IP address, it should be used instead of 127.0.1.1 )
echo -e 127.0.0.1'\t'localhost'\n'::1'\t\t'localhost'\n'127.0.1.1'\t'$hostName >> /etc/hosts

}

setUser () {

# Add regular user.
echo "Specify username. This will be used to identify your account on this machine."
read userName;
useradd -m -G wheel -s /bin/bash $userName

# Set password for new user.
echo "Specify password for regular user : $userName."
passwd $userName

# Enable sudo for wheel group.
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# Create directories for user.
pacman -S xdg-user-dirs --noconfirm; xdg-user-dirs-update

# Set the root password.
echo "Specify root password. This will be used to authorize root commands."
passwd

}

setGrub () {

# Install required packages.
pacman -S grub efibootmgr --noconfirm

# Create directory to mount EFI partition.
mkdir /boot/efi

# Mount the EFI partition.
mount /dev/sda1 /boot/efi

# Install grub.
grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi

# Generate grub config.
grub-mkconfig -o /boot/grub/grub.cfg

}

setShell () {

# Set terminal emulator (kitty).
pacman -S kitty --noconfirm
mkdir -p /home/$userName/.config/kitty; curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/.config/kitty/kitty.conf -o /home/$userName/.config/kitty/kitty.conf
mkdir -p /home/$userName/.config/kitty; curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/.config/kitty/current-theme.conf -o /home/$userName/.config/kitty/current-theme.conf

# Set shell (fish) and plugin manager (fisher).
pacman -S fish fisher
    
# Set theme for fish.
fish -c "fisher install IlanCosman/tide@v5"

# Fix hostname issue.
sed -i 's/hostname/uname -n/' /home/$userName/.config/fish/functions/fish_prompt.fish

# Set fish as default shell.
chsh --shell /bin/fish $userName
    
}

setEditor () {

# Install required packages.
pacman -S neovim nodejs npm --noconfirm

# Set neovim as default editor.
echo 'export VISUAL=nvim' | tee -a /etc/profile
echo 'export EDITOR=$VISUAL' | tee -a /etc/profile

}

setGUI () {

# Set display server (xorg).
pacman -S xorg --noconfirm

# Set display manager (lightdm).

pacman -S lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings --noconfirm
systemctl enable lightdm
mkdir -p /etc/lightdm; curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/.config/lightdm/lightdm-gtk-greeter.conf -o /etc/lightdm/lightdm-gtk-greeter.conf

# Set window manager (i3-gaps + i3blocks) and dependencies for custom config.

pacman -S i3-gaps i3blocks jq acpi brightnessctl --noconfirm
mkdir -p /home/$userName/.config/i3; curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/.config/i3/config -o /home/$userName/.config/i3/config
mkdir -p /home/$userName/.config/i3; curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/.config/i3/i3blocks.conf -o /home/$userName/.config/i3/i3blocks.conf

# Set notification daemon (dunst).
pacman -S dunst --noconfirm

# Set app launcher (rofi).
pacman -S rofi --noconfirm
mkdir -p /home/$userName/.config/rofi; curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/.config/rofi/config.rasi -o /home/$userName/.config/rofi/config.rasi

# Set wallpaper app (feh).
pacman -S feh --noconfirm
mkdir -p /home/$userName/Pictures; curl https://gitlab.com/sanganak_shilpkar/arch/-/raw/main/assets/wallpaper.png -o /home/$userName/Pictures/wallpaper.png

# Set appearance settings (lxappearance) and themes.
pacman -S lxappearance gnome-themes-extra papirus-icon-theme --noconfirm

}

setAudio () {

# Install audio.
pacman -S pulseaudio pulseaudio-alsa pulseaudio-bluetooth pavucontrol --noconfirm

}

setNetwork () {

# Install network.
pacman -S networkmanager --noconfirm
systemctl enable NetworkManager

}

setBluetooth () {

# Install bluetooth.
pacman -S blueman bluez bluez-utils --noconfirm
lsmod | grep btusb
rfkill unblock bluetooth
systemctl enable bluetooth.service

}

setSecurity () {

# Install firewall.
pacman -S gufw --noconfirm

}

setMaintenance () {

# Enable TRIM for SSDs
systemctl enable fstrim.timer

}

setOwner () {

# Set sane permissions for user files.
# ( This step is required as some files for the user are created/modified during install ~ as Root )

chown -R $userName /home/$userName/.config
chown -R :$userName /home/$userName/.config

chown -R $userName /home/$userName/Pictures
chown -R :$userName /home/$userName/Pictures

}

bugFixes() {

    # Reinstall pambase
    pacman -S pambase --noconfirm

}

# Update packages
pacman -Sy

# Install & configure.
setTime
setLocale   
setNetworkConfig
setUser
setGrub
setPackages
setEditor
setGUI
setAudio
setNetwork
setBluetooth
setSecurity
setMaintenance
setOwner
bugFixes

# Remove downloaded script & exit.
rm setup.sh
exit
